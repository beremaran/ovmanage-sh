#!/usr/bin/env bash

EASY_RSA_VERSION="3.0.5"
SERVER_NAME="mashivpn-server"

# install openvpn server
sudo apt update && \
sudo apt install openvpn;

# download easy-rsa
wget -P . https://github.com/OpenVPN/easy-rsa/releases/download/v${EASY_RSA_VERSION}/EasyRSA-nix-${EASY_RSA_VERSION}.tgz;
tar xvf EasyRSA-nix-${EASY_RSA_VERSION}.tgz;
mkdir easyrsa;
mv EasyRSA-${EASY_RSA_VERSION}/* easyrsa/;
rm -rf EasyRSA-${EASY_RSA_VERSION};

# configure certificates
cd easyrsa;
cp ../config/vars vars;
./easyrsa init-pki;
./easyrsa build-ca nopass
./easyrsa gen-req ${SERVER_NAME} nopass;
sudo cp pki/private/${SERVER_NAME}.key /etc/openvpn/;
./easyrsa import-req pki/reqs/${SERVER_NAME}.req ${SERVER_NAME};
./easyrsa sign-req server ${SERVER_NAME};
sudo cp pki/issued/${SERVER_NAME}.crt /etc/openvpn/;
sudo cp pki/ca.crt /etc/openvpn/;
./easyrsa gen-dh;
openvpn --genkey --secret ta.key;
sudo cp ta.key /etc/openvpn/;
sudo cp pki/dh.pem /etc/openvpn/;

# configure openvpn service
cd ..;
sudo cp config/server.conf /etc/openvpn/${SERVER_NAME}.conf;
sudo sed -i "s/#SERVER_NAME#/${SERVER_NAME}/g" /etc/openvpn/${SERVER_NAME}.conf;

# configure firewall (iptables)
sudo systemctl start iptables;
sudo systemctl enable iptables;

sudo iptables -A INPUT -i tun+ -j ACCEPT;
sudo iptables -A INPUT -i eth0 -m state --state NEW -p udp --dport 1194 -j ACCEPT;

sudo iptables -P FORWARD ACCEPT;
sudo iptables -A FORWARD -i tun+ -j ACCEPT;
sudo iptables -A FORWARD -i tun+ -o eth0 -m state --state RELATED,ESTABLISHED -j ACCEPT;
sudo iptables -A FORWARD -i eth0 -o tun+ -m state --state RELATED,ESTABLISHED -j ACCEPT;

sudo iptables -A OUTPUT -o tun+ -j ACCEPT;
sudo iptables -t nat -A POSTROUTING -s 10.8.0.0/24 -o eth0 -j MASQUERADE;

sudo iptables-save > /etc/iptables.conf;

# start & enable openvpn server
sudo systemctl restart openvpn;
sudo systemctl enable openvpn;
sudo systemctl restart openvpn@${SERVER_NAME};
sudo systemctl enable openvpn@${SERVER_NAME};
