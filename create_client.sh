#!/usr/bin/env bash

mkdir -p client-configs/keys;
mkdir -p client-configs/files;
chmod -R 700 client-configs;

CLIENT_NAME=$1

# generate certificate
cd easyrsa;
./easyrsa gen-req ${CLIENT_NAME} nopass;
cp pki/private/${CLIENT_NAME}.key ../client-configs/keys/;
./easyrsa import-req ../client-configs/keys/${CLIENT_NAME}.key;
./easyrsa sign-req client ${CLIENT_NAME};
cp pki/issued/${CLIENT_NAME}.crt ../client-configs/keys/;
cp ta.key ../client-configs/keys/;
sudo cp /etc/openvpn/ca.crt ../client-configs/keys;

# file permissions
sudo chown -R $USER:$USER ../client-configs;
sudo chmod -R 700 ../client-configs;

# generate *.ovpn
cd ..;
./make_config.sh ${CLIENT_NAME};
